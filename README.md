# Coap Web Firestore

Aplicação web em Angular para exibir, em tempo real, dados persistidos no Firestore.

## Guide

npm install
npm i @angular/fire
npm start

## Firebase

#### Deploy no Firebase

 - `npm install -g firebase-tools`
 - `firebase login`
 - `firebase init`
 - `ng build --prod`
 - `firebase deploy`
   - “What do you want to use as your public directory?” dist
   - "Configura as a single-page app? Yes
   - "File dist/index.html already exists. Overwrite? No

#### Alterar projeto Firebase
 - `firebase use --add`

#### Remover página em Firebase Hosting
 - `firebase hosting:disable`

## Ng2-charts
- `npm install ng2-charts@2.2.5 --save` - Versão compatível com Angular 7

## Construído com os repositórios

* [Sensor ESP8266 ESP-12](https://gitlab.com/monografia-kamila/coap-esp8266)
* [Front-end Angular](https://github.com/kamilaserpa/bluetooth-web-firestore) - *this project*
* [Java Coap Server](https://gitlab.com/monografia-kamila/coap-server)

### Repositórios para estudo: Client Iot Esp

 - https://github.com/kirintwn/Arduino-CoAP-Monitoring

## Author

Kamila Serpa - [Linked In](https://www.linkedin.com/in/kamila-serpa/), [Página/Resume](https://kamilaserpa.github.io/)

