import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MonoDocsComponent } from './mono-docs.component';

const routes: Routes = [
    {
        path: '',
        component: MonoDocsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MonoDocsRoutingModule {}
