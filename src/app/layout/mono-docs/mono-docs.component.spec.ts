import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonoDocsComponent } from './mono-docs.component';

describe('MonoDocsComponent', () => {
  let component: MonoDocsComponent;
  let fixture: ComponentFixture<MonoDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonoDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonoDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
