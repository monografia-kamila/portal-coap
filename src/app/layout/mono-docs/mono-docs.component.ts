import { Component, OnInit } from '@angular/core';

export interface Doc {
  title: string;
  subtitle: string;
  src: string;
  size: string;
}

@Component({
  selector: 'app-mono-docs',
  templateUrl: './mono-docs.component.html',
  styleUrls: ['./mono-docs.component.scss']
})
export class MonoDocsComponent implements OnInit {

  listDocs: Array<Doc> = [];

  constructor() { }

  ngOnInit() {
    this.listDocs = [
      {
        title: 'Diagrama de sequência', subtitle: 'Diagrama de sequência',
        src: '../../../assets/images/mono_diagrama_sequencia.png', size: 'big'
      },
      {
        title: 'Diagrama de rede', subtitle: 'Diagrama de rede',
        src: '../../../assets/images/diagrama_rede_coap.png', size: 'big'
      },
      {
        title: 'Formato da mensagem CoAP', subtitle: 'Formato da mensagem',
        src: '../../../assets/images/formato_da_mensagem.png', size: 'medium-h'
      },
      {
        title: 'Mensagem confiável (CON)', subtitle: 'Transmissão de mensagem confiável',
        src: '../../../assets/images/mensagem_confiavel.png', size: 'medium-w'
      },
      {
        title: 'Mensagem não confiável (NON)', subtitle: '',
        src: '../../../assets/images/mensagem_nao_confiavel.png', size: 'medium-w'
      },
      {
        title: 'Parâmetros de retransmissão', subtitle: '',
        src: '../../../assets/images/Parametros_retransmissao.png', size: 'medium-w'
      },
      {
        title: 'Estrutura de resposta', subtitle: '',
        src: '../../../assets/images/resposta_estrutura.png', size: 'small'
      },
      {
        title: 'Resposta NON', subtitle: '',
        src: '../../../assets/images/resposta_non.png', size: 'medium-w'
      },
      {
        title: 'Resposta Separada', subtitle: '',
        src: '../../../assets/images/resposta_separada.png', size: 'medium-w'
      },
      {
        title: 'Resposta Piggbacked', subtitle: '',
        src: '../../../assets/images/Solicitacao_resposta_piggbacked.png', size: 'medium-h'
      },
      {
        title: 'Resposta Piggbacked Fail', subtitle: '',
        src: '../../../assets/images/Solicitacao_resposta_piggbacked_fail.png', size: 'medium-h'
      },
      {
        title: 'Módulo DHT11', subtitle: '',
        src: '../../../assets/images/dht11_modulo.png', size: 'medium-h'
      },
      {
        title: 'Módulo ESP8266 Wemos D1 mini', subtitle: '',
        src: '../../../assets/images/esp_pin.png', size: 'big'
      },
      {
        title: 'Diagrama de conexões', subtitle: '',
        src: '../../../assets/images/conexoes.png', size: 'medium-h'
      },
      {
        title: 'Circuito', subtitle: '',
        src: '../../../assets/images/circuito.jpeg', size: 'medium-h'
      }

    ];
  }

  openLink(url: string) {
    window.open(url, '_blank');
  }

  // Filter on expansion-panel
  search(event: any) {
    let filter, ul, li, a, i, txtValue;
    filter = event.toUpperCase();
    ul = document.getElementById('myUL');
    li = ul.getElementsByTagName('mat-expansion-panel');
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName('mat-panel-title')[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = '';
      } else {
        li[i].style.display = 'none';
      }
    }
  }

}
