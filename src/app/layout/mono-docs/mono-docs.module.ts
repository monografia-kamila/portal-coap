import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonoDocsRoutingModule } from './mono-docs-routing.module';
import { MonoDocsComponent } from './mono-docs.component';
import { MatCardModule, MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule } from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MonoDocsComponent],
  imports: [
    CommonModule,
    MonoDocsRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatExpansionModule,
    SharedModule,
    MatInputModule,
    FormsModule,
    MatButtonModule
  ]
})
export class MonoDocsModule { }
