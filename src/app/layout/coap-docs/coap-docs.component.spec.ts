import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoapDocsComponent } from './coap-docs.component';

describe('CoapDocsComponent', () => {
  let component: CoapDocsComponent;
  let fixture: ComponentFixture<CoapDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoapDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoapDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
