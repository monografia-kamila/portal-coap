import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoapDocsComponent } from './coap-docs.component';

const routes: Routes = [
    {
        path: '',
        component: CoapDocsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoapDocsRoutingModule {}
