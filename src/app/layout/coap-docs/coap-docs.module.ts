import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoapDocsComponent } from './coap-docs.component';
import { CoapDocsRoutingModule } from './coap-docs-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CoapDocsComponent],
  imports: [
    CommonModule,
    CoapDocsRoutingModule,
    SharedModule
  ]
})
export class CoapDocsModule { }
