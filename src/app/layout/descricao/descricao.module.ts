import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DescricaoComponent } from './descricao.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DescricaoRoutingModule } from './descricao-routing.module';
import { MatCardModule } from '@angular/material';

@NgModule({
  declarations: [DescricaoComponent],
  imports: [
    CommonModule,
    SharedModule,
    DescricaoRoutingModule,
    MatCardModule
  ]
})
export class DescricaoModule { }
