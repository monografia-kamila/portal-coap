import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DescricaoComponent } from './descricao.component';

const routes: Routes = [
    {
        path: '',
        component: DescricaoComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DescricaoRoutingModule {}
