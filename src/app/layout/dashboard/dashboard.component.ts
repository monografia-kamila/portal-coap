import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';

import * as moment from 'moment';
import { Message } from 'src/app/shared/model/message.model';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    messagesCollection: AngularFirestoreCollection<Message>;
    messagesDoc: AngularFirestoreDocument<Message>;
    messages: Array<any> = [];

    dataSource: MatTableDataSource<Message>;

    flagTemperatura: any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private adb: AngularFireDatabase) {
        // BUSCA DADOS DO REALTIME DATABASE
        adb.list('messages').valueChanges().subscribe((data) => {
            this.messages = data.reverse();
            this.dataSource = new MatTableDataSource(this.messages);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });

        this.flagTemperatura = adb.object('buscaTemperatura/');

    }

    ngOnInit() {

    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    ativaNoTemperatura() {
//      (this.flagTemperatura.valueChanconsole.logges);
        this.flagTemperatura.set({ dateTime: moment().format('DD/MM/YYYY HH:mm:ss').toString() });
    }

}
