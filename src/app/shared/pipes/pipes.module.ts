import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TempPipe } from './temp.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [TempPipe],
    exports: [TempPipe]
})
export class PipesModule {}
