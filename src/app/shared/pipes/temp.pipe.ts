import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'temp'
})
export class TempPipe implements PipeTransform {

  transform(temperatura:any): any {
    return temperatura + '° C';
  }

}
