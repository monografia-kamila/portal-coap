export class Message {

    public temperatura: string;
    public umidade: string;
    public dateTime: string;
    public id?: string;

    constructor() {
        this.temperatura = '';
        this.umidade = '';
        this.dateTime = '';
    }
}
